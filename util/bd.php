<?php

/**
Crea una base de datos y reliza consutlas a la misma.
@class
@name bd
@param {string[]} $datos - Arreglo de datos de acceso a BD.
@returns none
*/
class bd{
	/**
	@property {string} svr - Nombre del servidor.
	@property {string} bd - Nombre de base de datos.
	@property {string} usr - Nombre de usuario.
	@property {string} pw - Contraseña de usuario.
	@property {string} $charset - conjunto de caracteres de la base de datos.
	@property {MysqlConnection} $con - Conexion con la base de datos.
	*/
	private $svr;
	private $bd;
	private $usr;
	private $pw;
	private $charset;

	public $con;

	function __construct($datos){
		$this->svr = $datos['svr'];
		$this->bd = $datos['bd'];
		$this->usr = $datos['usr'];
		$this->pw = $datos['pw'];
		$this->charset = 'utf8mb4';
	}

	/**
	Crea la conexion con la base de datos.
	@instance
	@function
	@returns none
	*/
	function conectar(){
		$this->con = new mysqli($this->svr, $this->usr, $this->pw,$this->bd);
		$this->con->set_charset($this->charset);
		if ($this->con->connect_errno > 0) {
			die('Fallo de apertura de conexion: ' . mysqli_connect_error());
		}
	}

	/**
	Elimina la conexion con la base de datos.
	@instance
	@function
	@returns none
	*/
	function desconectar(){
		$this->con->close();
		if ($this->con->connect_errno > 0) {
			die('Fallo de cierre de conexion: ' . mysqli_connect_error());
		}
	}

	/**
	Realiza una consulta a la base de datos, la consulta devuelve un conjunto de datos.
	@instance
	@function
	@param {string} $consulta - Consulta que se realizará. Puede tener campos para realizar reemplazos
	@param {string} $parametrosTipo - Cadena formada por los simbolos del tipo de valor que se almacena. (Ejemplo: 'isf' = int, string, float)
	@param {string[array]} $parametros - Arreglo con los valores a insertar como parametros en la consulta.
	@returns {object[]}
	*/
	function consultar($consulta, $parametrosTipo = null, $parametros = null){
		$retorno = null;
		try {
			bd::conectar();
			$consulta = $this->con->prepare($consulta);
			if(!is_null($parametros) && !is_null($parametrosTipo)){
				$consulta->bind_param($parametrosTipo, ...$parametros);
			}
			$consulta->execute();
			$resultado = $consulta->get_result();
			if(!$resultado){
				throw new Exception("Error en consulta.", 1);
			}
			$retorno = $resultado->fetch_all(MYSQLI_ASSOC);
			bd::desconectar();
		}
		catch(PDOException $e) {
			echo $sql . "<br>" . $e->getMessage();
		}
		return $retorno;
	}

	/**
	Ejecuta una consulta en la base de datos, la consulta NO devuelve un conjunto de datos.
	@instance
	@function
	@param {string} $consulta - Consulta que se realizará. Puede tener campos para realizar reemplazos
	@param {string} $parametrosTipo - Cadena formada por los simbolos del tipo de valor que se almacena. (Ejemplo: 'isf' = int, string, float)
	@param {string[array]} $parametros - Arreglo con los valores a insertar como parametros en la consulta.
	@returns none
	*/
	function ejecutar($consulta, $parametrosTipo = null, $parametros = null){
		$retorno = null;
		try {
		bd::conectar();
		$consulta = $this->con->prepare($consulta);
		if(!is_null($parametros) && !is_null($parametrosTipo)){
			$consulta->bind_param($parametrosTipo, ...$parametros);
		}
		$consulta->execute();
		bd::desconectar();
		}
		catch(PDOException $e) {
			echo $sql . "<br>" . $e->getMessage();
		}
	}
}
?>