<?php

/// Ubicacion relativa de la raiz de la API
//$root = $_SERVER['DOCUMENT_ROOT'].'/etc/bsale/api-php';
$root = $_SERVER['DOCUMENT_ROOT'].'../api';

/// Importacion
require $root.'/conf.php';
require $root.'/util/bd.php';

/// Instaciacion de base de datos
$bd1 = new bd(conf::$bd1);

///Obtencion de datos de la base de datos
if($_SERVER['REQUEST_METHOD'] == 'GET'){
	$resultado = $bd1->consultar('select id, name from category order by name');
	$resultado = str_replace("\/", "/", $resultado);
}
else{
	$resultado = 'Método no permitido';
}

/// Formateo y presentacion de los datos
$resultado = json_encode($resultado);
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
http_response_code(200);
echo $resultado;
?>