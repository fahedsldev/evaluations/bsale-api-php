<?php

/// Ubicacion relativa de la raiz de la API
//$root = $_SERVER['DOCUMENT_ROOT'].'/etc/bsale/api-php';
$root = $_SERVER['DOCUMENT_ROOT'].'/api';

/// Importacion
require $root.'/conf.php';
require $root.'/util/bd.php';

/// Instaciacion de base de datos
$bd1 = new bd(conf::$bd1);


///Obtencion de datos de la base de datos
if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['busqueda']) && !empty($_GET['busqueda']) ) {
	$busqueda = '%'.filter_var($_GET['busqueda'], FILTER_SANITIZE_STRING).'%';
	$resultado = $bd1->consultar('select p.id, p.name, price, url_image, discount, c.id cid, c.name category from product p join category c on p.category = c.id where p.name like(?) or c.name like(?)', 'ss', [$busqueda, $busqueda]);
	$resultado = str_replace("\/", "/", $resultado);
}
else if($_SERVER['REQUEST_METHOD'] == 'GET'){
	$resultado = $bd1->consultar('select p.id, p.name, price, url_image, discount, c.id cid, c.name category from product p join category c on p.category = c.id');
	$resultado = str_replace("\/", "/", $resultado);
}
else{
	$resultado = 'Método no permitido';
}

/// Formateo y presentacion de los datos
$resultado = json_encode($resultado);
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
http_response_code(200);
echo $resultado;
?>