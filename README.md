# **api-php**

## Ejercicio Completo
Construir una tienda online que despliegue productos agrupados por la categoría a
la que pertenecen, generando por separado backend (API REST) y frontend (aplicación
que la consuma) y utilizando la base de datos que se disponibiliza para su desarrollo.

## API REST
- Crear un buscador, el cual tiene que estar implementado a nivel de servidor, mediante una Api Rest cuyo lenguaje y framework puede ser de libre
elección.
- Agregar un buscador, implementado a nivel de servidor.
- Finalmente, disponibilizar la aplicación y el repositorio con el código en un hosting.

## Ejecución
Se creó la API REST en PHP si frameworks y se puso a disponibilidad en un [VPS no administrado](https://algorit.io/etc/bsale/api.php).